### What is this repository for? ###

# README #

* Quick summary 伴随着移动终端“蓝牙”、“无线局域网”等近距离通讯功能模块的普及，移动终端局域网通讯应用是一个面向无移动网络和WiFi的紧急情况下一个临时通讯需求的应用。移动终端局域网通讯应用的优势在于市面上还未出现一种软件仅通过蓝牙、无线局域网来实现近距离的通话、文字聊天、文件传输以及语音对讲功能，并且方便，快捷，操作简单，所占内存极小。

* Version 1.1

1. 本应用面向紧急情况和临时的民用短、中距离无线通讯，如：突发自然灾害，出现移动基站瘫痪，同时附近无可用无线局域网的情况下，移动终端局域网通讯或许会成为唯一的求生方式；在外地漫游又找不到WiFi想同小伙伴传送文件；不想买昂贵的对讲机临时又需要对讲。
2. 应用所占用内存小，待机状态下不占用系统资源，用户可安装作为备用，不影响日常通信，后期可整合至各厂商ROM作为系统功能之一，简单快捷。
3. UI使用最新的Material Design设计语言，界面美观，操作简单方便。
4. 代码使用Android Studio IDE编写，遵守Andriod开发规范。
5. 完全不需要借助外部WiFi热点或移动数据。
6. WiFi模式下可多人组建一个大的局域网聊天室，拥有以上功能同时，可以考虑加入一些多人对战娱乐功能。
7. 电脑遥控手机拍照，手机遥控电脑鼠标等各种神奇科技（也是同局域网内）

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* iMaeGoo [mail1st#qq.com]
* Cao Yan, Li Ning, Liu Wenjing